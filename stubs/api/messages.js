const router = require('express').Router();

router.get('/message/:appid', (req, res) => {
    res.send(require('./json/appmessages/success.json'))
})

router.post('/message/create', (req, res) => {
    res.send({})
})

router.post('/locales/create', (req, res) => {
    res.send({})
})

module.exports = router;
