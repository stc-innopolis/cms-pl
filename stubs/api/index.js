const router = require('express').Router();
const { v4: uuid } = require('uuid');
const fs = require('fs');
const path = require('path');
const messagesRouter = require('./messages');

router.use(messagesRouter);

router.get('/apps', (req, res) => {
    const pathRes = path.resolve(__dirname, './json/apps/applist/success.json');
    fs.readFile(pathRes, (error, apps) => {
        res.send(JSON.parse(apps));
    })
})

router.post('/apps/create', (req, res) => {
    const pathRes = path.resolve(__dirname, './json/apps/applist/success.json');
    fs.readFile(pathRes, (error, apps) => {
        const data = JSON.parse(apps);
        const newApp = {
            "id": uuid(),
            "appName": req.body.appName,
            "statistic": {
                "totalText": 0,
                "totalFile": 0,
                "request": 0
            },
            "users": 1,
            "favorite": false
        }
        data.data.push(newApp);
        fs.writeFile(pathRes, JSON.stringify(data, null, 4), {}, () => {
            res.send({data: newApp});
        });
    })
})

router.get('/app-statistic/:appid', (req, res) => {
    res.send(require('./json/dashboard/success.json'))
})

module.exports = router;

