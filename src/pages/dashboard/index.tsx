import React, { useState } from 'react';
import { useParams } from 'react-router-dom';

import { star, starFilled } from '../../assets';
import { useAddAppMutation, useAppStatisticQuery, useDelAppMutation } from '../../services/apps';
import { URLs } from '../../__data__/urls';
import { Layout, Typography, Button } from '../../components';

import {
    AppCard,
    CardIconsWrapper,
    InfoWrapper,
    CardsWrapper,
    ProgressBarCircle,
    ProgressBarWrapper,
    ProgressBarPercent,
    JustWrapper
} from './style';


const DelApp = () => {
    const [delapp, {
        isLoading
    }] = useDelAppMutation();
    const [appName, setAppName] = useState('');
 
    return(     
        <Button color='danger' onClick={() => setAppName(appName)}>Удалить</Button>
    );
};


export const Dashboard = () => {
    const params = useParams();
    const { isLoading, data, error } = useAppStatisticQuery(params.appId);
    const textProgress = (data?.data.statistic.totalText || 0)/(data?.data.limits.maxTexts || 0);
    const fileProgress = (data?.data.statistic.totalFileSize || 0)/(data?.data.limits.maxSumSizeFiles || 0);
    return (
        <Layout>
            <JustWrapper> 
                <Typography.Header1>{data?.data.appName}</Typography.Header1>
                <DelApp/>
            </JustWrapper>
            <CardsWrapper>
                <AppCard to={URLs.files.getUrl(params.appId)}>
                    <h2>Файлы</h2>
                    <InfoWrapper><p>запросов за месяц</p> <p>{data?.data.statistic.requestMonthFile}</p> </InfoWrapper>
                    <InfoWrapper><p>всего файлов</p> <p>{data?.data.statistic.totalFile}</p> </InfoWrapper>
                    <InfoWrapper><p>запрошено за 24 часа</p> <p>{data?.data.statistic.request24h}</p> </InfoWrapper>
                    <CardIconsWrapper>
                        <img src={data?.data.favorits.file ? starFilled : star} alt="" />
                    </CardIconsWrapper>
                    <ProgressBarWrapper>
                        <svg>
                            <ProgressBarCircle progress={fileProgress}/>
                        </svg>
                        <ProgressBarPercent>{Math.round(fileProgress * 10000)/100}%</ProgressBarPercent>
                    </ProgressBarWrapper>
                </AppCard>
                <AppCard to={URLs.messages.getUrl(params.appId)}>
                    <h2>Текстовки</h2>
                    <InfoWrapper><p>запросов за месяц</p> <p>{data?.data.statistic.requestMonthText}</p> </InfoWrapper>
                    <InfoWrapper><p>всего констант</p> <p>{data?.data.statistic.totalText}</p> </InfoWrapper>
                    <InfoWrapper><p>запрошено за 24 часа</p> <p>{data?.data.statistic.request24h}</p> </InfoWrapper>
                    <CardIconsWrapper>
                        <img src={data?.data.favorits.text ? starFilled : star} alt="" />
                    </CardIconsWrapper>
                    <ProgressBarWrapper>
                        <svg>
                            <ProgressBarCircle progress={textProgress}/>
                        </svg>
                        <ProgressBarPercent> {Math.round(textProgress * 10000)/100}% </ProgressBarPercent>
                    </ProgressBarWrapper>
                </AppCard>
            </CardsWrapper>
        </Layout>
    );
};
