import styled from '@emotion/styled';
import { Link } from 'react-router-dom';

export const AppCard = styled(Link)`
    text-decoration: none;
    color: #000;
    position: relative;
    background: #FFEEE2;
    box-shadow: 4px 4px 10px rgba(0, 0, 0, 0.19);
    border-radius: 19px;
    padding: 32px;
    width: 600px;
    margin-top: 46px;
`;

export const CardIconsWrapper = styled.div`
    position: absolute;
    top: 24px;
    right: 24px;
    display: flex;
    flex-direction: column;
    align-items: center;
`;

export const InfoWrapper = styled.div`
    display: flex;
    width: 270px;
    justify-content: space-between;
    line-height: 20%;
`;

export const JustWrapper = styled.div`
    display: flex;
    justify-content: flex-start;
    gap: 30px;
    height: 50px;
    align-content: center;
    align-items: center;
    margin-top: 64px;
`;

export const CardsWrapper = styled.div`
    display: flex;
    justify-content: flex-start;
    gap: 64px;
    flex-wrap: wrap;
`;

export const ProgressBarWrapper = styled.div`
    width: 150px;
    height: 150px;
    position: absolute;
    top: 65px;
    right: 32px;
    display: flex;
    justify-content: center;
    align-items: center;
`;

type ProgressBarCircleProps = {
    progress: number;
};

export const ProgressBarCircle = styled.circle<ProgressBarCircleProps>`
    stroke-dasharray: 421;
    stroke-dashoffset: -${(props) => (1-props.progress) * 421};
    transform: scale(1,-1) rotate(90deg);
    transform-origin: center;
    fill: transparent;
    stroke: #6B9DAD;
    stroke-width: 9;
    cx: 75;
    cy: 75;
    r: 67;
`;

export const ProgressBarPercent = styled.div`
    position: absolute;
    display: flex;
    font-weight: 700;
    width: 24px;
`;