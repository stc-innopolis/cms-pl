import React, { useState } from 'react';

import { star, users, starFilled } from '../../assets';
import { useAppsListQuery, useAddAppMutation } from '../../services/apps';
import { URLs } from '../../__data__/urls';
import { Layout, Input, Typography, Button } from '../../components';

import {
    Form,
    AppCard,
    CardIconsWrapper,
    InfoWrapper,
} from './style';

const AddApp = () => {
    const [addapp, {
        isLoading
    }] = useAddAppMutation();
    const [appName, setAppName] = useState('');
    const handleSubmit = event => {
        event.preventDefault();
        addapp(appName)
    }

    return(     
        <Form onSubmit={handleSubmit}>
            <Input value={appName} onChange={event => setAppName(event.target.value)} />
            <Button>Добавить</Button>
        </Form>
    );
};

export const Apps = () => {
    const { isLoading, data, error } = useAppsListQuery();

    return (
        <Layout>
            <Typography.Header1>Список приложений</Typography.Header1>
            <AddApp/>
            {data?.data.map((app) => (
                <AppCard to={URLs.dashboard.getUrl(app.id)} key={app.id}>
                    <h2>{app.appName}</h2>
                    <InfoWrapper><p>всего текстовок</p> <p>{app.statistic.totalText}</p> </InfoWrapper>
                    <InfoWrapper><p>всего файлов</p> <p>{app.statistic.totalFile}</p> </InfoWrapper>
                    <InfoWrapper><p>запрошено за 24 часа</p> <p>{app.statistic.request24h}</p> </InfoWrapper>
                    <CardIconsWrapper>
                        <img src={app.favorite ? starFilled : star} alt="" />
                        <img src={users} alt="" />
                        <p>{app.users}</p>
                    </CardIconsWrapper>
                </AppCard>
            ))}
        </Layout>
    );
};


