import styled from '@emotion/styled';
import { Link } from 'react-router-dom';

export const AppCard = styled(Link)`
    position: relative;
    background: #FFEEE2;
    box-shadow: 4px 4px 10px rgba(0, 0, 0, 0.19);
    border-radius: 19px;
    padding: 32px;
    width: 600px;
    margin-top: 46px;
    display: block;
    text-decoration: none;
    color: #000000;
    
`;

export const Form = styled.form`
    display: flex;
`;

export const CardIconsWrapper = styled.div`
    position: absolute;
    top: 24px;
    right: 24px;
    display: flex;
    flex-direction: column;
    align-items: center;
`;

export const InfoWrapper = styled.div`
    display: flex;
    width: 270px;
    justify-content: space-between;
    line-height: 20%;
`;