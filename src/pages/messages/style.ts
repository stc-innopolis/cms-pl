import styled from '@emotion/styled';

export const Form = styled.form`
    display: flex;
`;

export const MessagesIconsWrapper = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    gap: 32px;
    margin-right: 32px;
    margin-left: auto;
`;

export const Views = styled.div`
    display: flex;
    gap: 12px;
`;

export const ListMessages = styled.ul`
    font-size: 24px;
    padding-left: 0;
    display: flex;
    flex-direction: column;
    gap: 8px;
`;

export const ListItem = styled.li`
    cursor: pointer;
    list-style: none;
    padding-left: 24px;
    display: flex;
    align-items: center;
    border: 2px solid #b6b6b6;
    border-radius: 5px;
`;

export const MessageKey = styled.p`
    margin-left: 8px;
`;
