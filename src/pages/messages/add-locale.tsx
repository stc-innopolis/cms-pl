import React, { useState } from 'react';

import { useAddLocaleMutation } from '../../services/apps';
import { Input, Button } from '../../components';

import { Form } from './style';

type AddLocaleProps = {
    messageId: string;
}

export const AddLocale: React.FC<AddLocaleProps> = ({ messageId }) => {
    const [addLocale, {
        isLoading, ...other
    }] = useAddLocaleMutation();
    const error = other.error?.data?.error
    const [language, setLanguage] = useState('');
    const [value, setValue] = useState('');
    const handleSubmit = event => {
        event.preventDefault();
        addLocale({ messageId, language, locale: value });
    };

    return(     
        <Form onSubmit={handleSubmit}>
            {error && (
                <div>
                    {error}
                </div>
            )}
            <Input placeholder='Язык' value={language} onChange={event => setLanguage(event.target.value)}/>
            <Input placeholder='Значение' value={value} onChange={event => setValue(event.target.value)}/>
            <Button>Добавить</Button>
        </Form>
    );
};

