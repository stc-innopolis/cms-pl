import React, { useState } from 'react';

import { useAddMessageMutation } from '../../services/apps';
import { Input, Button } from '../../components';

import { Form } from './style';

type AddMessageProp = {
    appId: string;
}

export const AddMessage: React.FC<AddMessageProp> = ({ appId }) => {
    const [addMessage, {
        isLoading
    }] = useAddMessageMutation();
    console.log()
    const [messageName, setMessageName] = useState('');
    const handleSubmit = event => {
        event.preventDefault();
        addMessage({messageName, appId});
    };

    return(     
        <Form onSubmit={handleSubmit}>
            <Input placeholder='Ключ константы' value={messageName} onChange={event => setMessageName(event.target.value)}/>
            <Button>Добавить</Button>
        </Form>
    );
};

