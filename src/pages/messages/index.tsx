import React, { useState } from 'react';
import { useParams } from 'react-router-dom';

import { eye, trash, chevron } from '../../assets';
import { useMessagesQuery, useAppsListQuery } from '../../services/apps';
import { Layout, Typography, Collapse } from '../../components';

import { AddMessage } from './add-message';
import { AddLocale } from './add-locale';

import {
    MessagesIconsWrapper,
    ListMessages,
    ListItem,
    Views,
    MessageKey,
} from './style';

export const Messages = () => {
    const params = useParams();
    const { isLoading, data, error } = useMessagesQuery(params.appId);
    const { isLoading: appsIsLoading, data: appsListData, error: appsListError } = useAppsListQuery();
    const [openedChevron, setOpenChevron] = useState(null);
    return (
        <Layout>
            <Typography.Header1>{appsListData?.data?.find(app => app.id === params.appId).appName}</Typography.Header1>
            <AddMessage appId={params.appId}/>
            <ListMessages>
                {data?.data?.map((message) => (
                    <Collapse
                        key={message.id}
                        onToggle={() => setOpenChevron(id => message.id === id ? null : message.id)}
                        open={message.id === openedChevron}
                        header={(
                            <>
                                <MessageKey>{message.messageKey}</MessageKey>
                                <MessagesIconsWrapper>
                                    <Views>
                                        <img src={eye} alt="" />
                                        <p>{message?.statistic?.requestTotalText}</p>
                                    </Views>
                                    <img src={trash} alt="" />
                                </MessagesIconsWrapper>
                            </>
                        )}
                        body={(
                            <>
                                <AddLocale messageId={message.id}/>
                            </>
                        )}
                    />
                    
                ))}
            </ListMessages>
        </Layout>
    );
};
    