import React from 'react';

import { useAppsListQuery } from '../../services/apps';
import { Layout, Typography } from '../../components';

import {
} from './style';

export const Files = () => {
    const { data } = useAppsListQuery();

    return (
        <Layout>
            <Typography.Header1>{data?.data.appName}</Typography.Header1>
        </Layout>
    );
};
    