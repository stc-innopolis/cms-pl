// Need to use the React-specific entry point to import createApi
import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { getConfigValue } from '@ijl/cli'

// Define a service using a base URL and expected endpoints
export const appsApi = createApi({
    reducerPath: 'appsApi',
    tagTypes: ['App', 'Messages'],
    baseQuery: fetchBaseQuery({ baseUrl: getConfigValue('ijl-cms.api') }),
    endpoints: (builder) => ({
        appsList: builder.query<any, void>({
            query: () => '/apps',
            providesTags: ['App'],
        }),
        addApp: builder.mutation<any, string>({
            query: (appName) => ({
                url: '/apps/create',
                method: 'POST',
                body: {
                    appName,
                }
            }),
            invalidatesTags: ['App'],
        }),
        DelApp: builder.mutation<any, string>({
            query: (appName) => ({
                url: '/apps/delete',
                method: 'DELETE',
                body: {
                    appName,
                }
            }),
            invalidatesTags: ['App'],
        }),
        appStatistic: builder.query<any, string>({
            query: (id) => `/app-statistic/${id}`,
        }),
        messages: builder.query<any, string>({
            query: (appId) => `/message/${appId}`,
            providesTags: ['Messages'],
        }),
        addMessage: builder.mutation<any, Record<string, string>>({
            query: ({ messageName, appId }) => ({
                url: '/message/create',
                method: 'POST',
                body: {
                    messageKey: messageName,
                    appId
                }
            }),
            invalidatesTags: ['Messages'],
        }),
        addLocale: builder.mutation<any, Record<string, string>>({
            query: ({ messageId, languagekey, locale }) => ({
                url: `/message/${messageId}/locales/create`,
                method: 'POST',
                body: {
                    languagekey,
                    locale
                }
            }),
            invalidatesTags: ['Messages'],
        }),
    }),
});

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const { useAppsListQuery, useAddAppMutation, useDelAppMutation, useAppStatisticQuery, useMessagesQuery, useAddMessageMutation, useAddLocaleMutation } = appsApi;