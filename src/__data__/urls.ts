import { getNavigations, getNavigationsValue } from '@ijl/cli';
import pkg from '../../package.json';

const baseUrl = getNavigationsValue(`${pkg.name}.main`);
const navs = getNavigations();
const makeUrl = url => baseUrl + url;

export const URLs = {
    baseUrl,
    apps: {
        url: baseUrl,
    },
    dashboard: {
        url: makeUrl(navs['ijl-cms.app.dashboard']),
        getUrl(id) {
            return makeUrl(navs['ijl-cms.app.dashboard']).replace(':appId', id);
        }
    },
    messages: {
        url: makeUrl(navs['ijl-cms.app.messages']),
        getUrl(id) {
            return makeUrl(navs['ijl-cms.app.messages']).replace(':appId', id);
        }
    },
    files: {
        url: makeUrl(navs['ijl-cms.app.files']),
        getUrl(id) {
            return makeUrl(navs['ijl-cms.app.files']).replace(':appId', id);
        }
    }
};
