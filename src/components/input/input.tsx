import React from 'react';

import { Field as StyledField, type FieldProps as StyledFieldProps } from './style';

type FieldProps = StyledFieldProps & React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>;

export const Input = (props: FieldProps) => (
    <StyledField {...(props)} />
);

Input.defaultProps = {
    size: 'large',
};
