import styled from '@emotion/styled';

export type FieldProps = {
    size: 'large' | 'small'
}

export const Field = styled.input<FieldProps>(({ size }) => `
    min-width: ${size === 'large' ? '360px' : '68px'} ;
    height: 51px;
    font-size: 25px;
    background: #FFFFFF;
    border: 1px solid #C4C4C4;
    box-sizing: border-box;
    border-radius: 5px;
`);
