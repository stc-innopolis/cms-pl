import styled from '@emotion/styled';

type Indent = 'zero' | 'default';

type IndentsProps = {
    indent?: Indent;
};

export const Header1 = styled.h1<IndentsProps>`
    font-style: normal;
    font-weight: 400;
    font-size: 48px;
    line-height: 58px;
    display: flex;
    align-items: center;
    text-align: center;
    margin: ${({ indent = 'default' }) => (indent === 'default' ? '64px' : 0)} 0;
`;

export const Header2 = styled.h2<IndentsProps>`
    font-weight: 400;
    font-size: 36px;
    line-height: 42px;
    margin: ${({ indent = 'default' }) => (indent === 'default' ? '32px' : 0)} 0;
`;

export const Typography = styled.p<IndentsProps>`
    font-style: normal;
    font-weight: 400;
    font-size: 16px;
    line-height: 19px;
    margin: ${({ indent = 'default' }) => (indent === 'default' ? '24px' : 0)} 0;
`;
