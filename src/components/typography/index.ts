import { Typography as baseTypography, Header1, Header2 } from './typography';

type Additional = {
    Header1?: typeof Header1;
    Header2?: typeof Header2;
};

const Component: typeof baseTypography & Additional = baseTypography;

Component.Header1 = Header1;
Component.Header2 = Header2;

const Typography: typeof baseTypography & Required<Additional> =
    Component as typeof baseTypography & Required<Additional>;

export default Typography;
