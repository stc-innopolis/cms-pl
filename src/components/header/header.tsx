import React from 'react';
import { Link } from 'react-router-dom';

import { logoDark, user } from '../../assets';
import { URLs } from '../../__data__/urls';

import { Header as StyledHeader } from './style';

export const Header = () => {
    return (
        <StyledHeader>
            <Link to={URLs.apps.url}>
                <img src={logoDark} alt="logo" />
            </Link>
            <img src={user} alt="" />
        </StyledHeader>
    );
};
