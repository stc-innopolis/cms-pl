import styled from '@emotion/styled';
import { Link } from 'react-router-dom';

export const Header = styled.header`
    height: 96px;
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.05);
    background: #FFFFFF;
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding-left: 24px;
    padding-right: 32px;
`;
