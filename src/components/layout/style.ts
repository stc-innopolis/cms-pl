import styled from '@emotion/styled';

export const Wrapper = styled.div`
    height: 100%;
`;

export const Main = styled.main`
    height: 100%;
    max-width: 1600px;
    margin: 0 auto;
    padding: 0 64px;
`;
