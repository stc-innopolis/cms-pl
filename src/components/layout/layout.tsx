import React from 'react';

import { Header } from '../header';

import { Wrapper, Main } from './style';

export const Layout = ({ children }) => {
    return (
        <Wrapper>
            <Header />
            <Main>
                {children}
            </Main>
        </Wrapper>
    )
}
