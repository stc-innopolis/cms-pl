import styled from '@emotion/styled';

export const Wrapper = styled.div`
    cursor: pointer;
    list-style: none;
    padding-left: 24px;
    display: flex;
    flex-direction: column;
    border: 2px solid #b6b6b6;
    border-radius: 5px;
`;

export const Header = styled.div`
    display: flex;
    align-items: center;
`;

export const Body = styled.div`

`;

type ChevronProps = {
    open: boolean;
}

export const Chevron = styled.img<ChevronProps>`  
    transform: rotate(${({ open }) => open ? '0':'180deg'});
    transition: transform 0.5s;
`;
