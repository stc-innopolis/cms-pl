import React from 'react';

import { chevron } from '../../assets';

import {
    Wrapper,
    Body,
    Header,
    Chevron,
} from './style';

type CollapseProps = {
    header: React.ReactNode;
    body: React.ReactNode;
    open: boolean;
    onToggle: () => void;
    as?: 'div' | 'li';
}

export const Collapse: React.FC<CollapseProps> = ({ body, header, onToggle, open, as }) => {
    const WrapperWithTag = Wrapper.withComponent(as)
    return (
        <WrapperWithTag>
            <Header onClick={onToggle}>
                <Chevron open={open} src={chevron} alt="" />
                {header}
            </Header>
            {open && (
                <Body>{body}</Body>
            )}
        </WrapperWithTag>
    )
}

Collapse.defaultProps = {
    as: 'div',
}