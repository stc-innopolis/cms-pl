import styled from '@emotion/styled';

export type ButtonProps = {
    size: 'medium' | 'small',
    color: 'accept' | 'danger' 
}

export const Button = styled.button<ButtonProps>(({ size, color }) => `
    padding: ${size === 'medium' ? '16px 24px' : '8px'};
    margin-left: 8px;
    background-color: ${color === 'accept' ? '#51FBA6' : '#FFA3A3'};
    border-radius: 4px;
    border: none;
    font-style: normal;
    font-weight: 400;
    font-size: 16px;
    line-height: 19px;
    text-align: center;
`);