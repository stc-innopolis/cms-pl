import React from 'react';

import { Button as StyledButton, type ButtonProps as StyledButtonProps } from './style';

type ButtonProps = StyledButtonProps & React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement>

export const Button: React.FC<Partial<ButtonProps>> = (props) => (
    <StyledButton {...(props as ButtonProps)} />
);

Button.defaultProps = {
    size: 'medium',
    color: 'accept',
};
