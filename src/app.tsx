import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { injectGlobal } from '@emotion/css';
import { ApiProvider } from '@reduxjs/toolkit/query/react';

import { Dashboard } from './dashboard';
import { appsApi } from './services/apps';

injectGlobal`
    @import url('https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap');

    body, #app, html {
        font-family: 'Roboto', sans-serif;
        width: 100%;
        height: 100%;
    }
`;

const App = () => {
    return(
        <ApiProvider api={appsApi}>
            <BrowserRouter>
                <Dashboard />
            </BrowserRouter>
        </ApiProvider>
    );
};

export default App;

