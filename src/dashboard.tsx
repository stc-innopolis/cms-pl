import React from 'react';
import { Routes, Route } from 'react-router-dom';

import { URLs } from './__data__/urls';
import {
    Apps,
    Dashboard as DashboardPage,
    Files,
    Messages
} from './pages';


export const Dashboard = () => {
    return (
        <Routes>
            <Route path={URLs.apps.url} element={<Apps />} />
            <Route path={URLs.dashboard.url} element={<DashboardPage />} />
            <Route path={URLs.messages.url} element={<Messages />} />
            <Route path={URLs.files.url} element={<Files />} />

            <Route path="*" element={<h1>Not found</h1>} />
        </Routes>
    );
};
