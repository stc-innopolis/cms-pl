

const igor = {
    name: 'Igor',
    old: 25,
}

const oleg = {
    name: 'oleg',
    old: 32,
    children: igor,
}

const vasya = {
    ...oleg,
    children: {
        ...oleg.children
    }
}
// const vasya = oleg
vasya.name = 'Vasya'
vasya.children.name = 'Leha'
// console.log(vasya)

const { old, name, ...other } = oleg
console.log(oleg)
console.log(vasya)
console.log(igor)