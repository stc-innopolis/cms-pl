const pkg = require('./package')

module.exports = {
    apiPath: 'stubs/api',
    webpackConfig: {
        output: {
            publicPath: `/static/${pkg.name}/${process.env.VERSION || pkg.version}/`
        }
    },
    navigations: {
        "ijl-cms.main": "/ijl-cms",
        "ijl-cms.app.dashboard": "/:appId/dashboard",
        "ijl-cms.app.messages": "/:appId/messages",
        "ijl-cms.app.files": "/:appId/files"
    },
    features: {
        'ijl-cms': {
            // add your features here in the format [featureName]: { value: string }
        },
    },
    config: {
        'ijl-cms.api': 'http://localhost:8045/v1',
        // 'ijl-cms.api': '/api',
    }
}
